# [SECTION] Lists
# Python alter ego of ARRAY
names = ["John", "Paul", "Goerge", "Ringo"]
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# to get length of list
print(len(programs))
# to get item from list
print(programs[0])
# gets certain items from list
# just like splice()
print(programs[0:2])

print(programs[2])
# update specific element
programs[2] = "Short Courses"

print(programs[2])

# [SECTION] List Manipulation
# Adds a new item onto the list
durations.append(367)
print(durations)

# Deleting an item from the list
del durations[-1] # popped off

print(durations)

# sorts list ascending
names.sort()
print(names)

# sorts list descending
names.sort(reverse=True)
print(names)

# for clearing/emptying the list
test_list = [1, 3, 5, 7, 9]
test_list.clear()
print(test_list)

# [SECTION] Dictionaries
# Python alter ego of OBJECT
person1 = {
	"name": "Elon",
	"age": 70,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}
# length
print(len(person1))
# specific value based on key
print(person1["name"])
# all keys
print(person1.keys())
# all values
print(person1.values())
# all items (both key and value)
print(person1.items())

# [SECTION] Disctionary Manipulation
# same but diff
person1["nationality"] = "Norwegian"
# update() = adds new item (key-value pair)
person1.update({"fave_food": "Dinuguan"})
print(person1)

# pop() = removes item
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear all entries
person2 = {
	"name": "Mystika",
	"age": 18
}
person2.clear()
print(person2)

# Nested Dictionaries
person3 = {
	"name": "Monika",
	"age": 21,
	"occupation": "Lyricist",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Ruby"]
}

classRoom = {
	"student1": person1,
	"student2": person3
}

# [SECTION] Functions
# def = declares/defines a function
# useful for bulk codes
def my_greeting():
	print("Hello user!")

# call function
my_greeting()

def greet_user(username):
	print(f"Hello, {username}")

greet_user("Elon")

# Lambda Functions
# useful for short functions
# anonymous function (not defined) just like arrow function(?)
greeting =  lambda person : f"Hello {person}!"
print(greeting("Elise"))
print(greeting("Tony"))

# [SECTION] Classes
# Python alter ego of OBJECT CONSTRUCTOR
class Car():
	def __init__(self, brand, model, year_of_make):
		# self is declared 'this'
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		# Other properties
		self.fuel = "Gasoline"
		self.fuel_level = 0

	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print("...filling up the fuel tank")
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")
# MINI ACTIVITY
# Declare a drive function for the Car class which will print the total distance that the car has already driven
# Along with that, also print the fuel level minus the distance that the car has driven
# *NOTE: No need for user input, just hardcode the distance
	def drive(self, distance):
		if distance >= self.fuel_level:
			print(f"You have driven a total of {self.fuel_level} miles and ran out of fuel.")
			self.fuel_level = 0
		elif distance < 0:
			self.fuel_level += distance
			print(f"You have driven backwards a total of {-distance} miles")
		else:
			self.fuel_level -= distance
			print(f"You have driven a total of {distance} miles")
		print(f"New fuel level: {self.fuel_level}")

new_car = Car("Maserati", "Explorer", "2005")
print(f"My car is a {new_car.brand} {new_car.model}")
new_car.fill_fuel()
new_car.drive(90)
new_car.fill_fuel()
new_car.drive(0)
new_car.fill_fuel()
new_car.drive(110)
new_car.fill_fuel()
new_car.drive(-50)